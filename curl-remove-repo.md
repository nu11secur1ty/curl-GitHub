# Remove repo using "curl" on GitHub

- your_username: your
- your_repo: your
```
curl -X DELETE -u your_username https://api.github.com/repos/{your_username}/{your_repo}
```
- FYI

Here, you use already created tokens to delete your repo.

# Have fun with nu11secur1ty =)
